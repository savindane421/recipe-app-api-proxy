# Receipe App API proxy

NGINX proxy app for our receipe app API

## Usage

### Environment Variables
* `LISTION_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of app to forward request (default: `app`)
* `APP_PORT` - Port to app forword requests (default: `9000`)
